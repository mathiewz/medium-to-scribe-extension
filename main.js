browser.contextMenus.create({
    id: "read-on-scribe",
    title: "Read this on scribe",
    documentUrlPatterns: ["https://medium.com/*/*"]
});

browser.contextMenus.onClicked.addListener((info, tab) => {
    if (info.menuItemId === "read-on-scribe") {
        browser.tabs.executeScript({
            file: "redirect-to-scribe.js",
        });
    }
});